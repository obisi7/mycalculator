import React, { Component } from 'react';
import { View, Text } from 'react-native';

// import { StyleSheet, Text, View } from 'react-native';
// import { Display } from "./components/widgets/Display";
import { HomeScreen } from "./screens/HomeScreen";


class App extends Component {

  render() {
    return (
      // <View>
      //   <Text> Hi there </Text>
      //   <Text> Yes  </Text>
      //   <Text> Yes  </Text>
      //   <Text> Yes  </Text>
      //   <Text> Hi there </Text>
      // </View>
      <HomeScreen />
    )
  }
}

const styles = {
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
}

export default App;