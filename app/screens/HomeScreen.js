import React, { Component, Fragment } from 'react';
import { StyleSheet, SafeAreaView, Dimensions, Platform, StatusBar, ImageBackground, PanResponder, View, Text } from 'react-native';
import { Display, Button, Header, Footer } from './../components/widgets';
// import { Platform } from 'expo-core';

// const calcButtons = [
//   ["C", "±", "%", "Del"],
//   ["7", "8", "9", "÷"],
//   ["4", "5", "6", "x"],
//   ["3", "2", "1", "-"],
//   ["0", ".", "=", "+"],
// ]

const unaryButtons = [
  ["C", "±", "%", "Del"]
]

const calcButtons = [
  ["7", "8", "9"],
  ["4", "5", "6"],
  ["1", "2", "3"], // numbers re-arranged to mimic android calculator
  ["0", ".", "="]
]

const binaryButtons = [
  ["/", "*", "-", "+"]
]
const appBackgroundImage = require("../../assets/images/morganBg.png");


export  class HomeScreen extends Component {

  constructor(props) {
    super(props);

    this.initialState = {
      displayValue: "0",
      result: "0",
      calculationDone: false,
      // operator: null,
      history: "",
      orientation: "portrait",
      firstValue: "",
      secondValue: "",
      isDot: false, //a period entered?
      nextValue: false, // boolean to dertermine if more digits are entered for calculation
      
    }
    this.state = this.initialState;

    // Listen for orientation changes...
    Dimensions.addEventListener('change', () => {
      const { width, height } = Dimensions.get("window");
      var orientation = (width > height) ? "landscape" : "portrait";
      this.setState({ orientation: orientation });
    });

    // Setup gestures...
    // this.panResponder = PanResponder.create({
    //   onStartShouldSetPanResponder: (evt, gestureState) => true,
    //   onStartShouldSetPanResponderCapture: (evt, gestureState) => true,
    //   onMoveShouldSetPanResponder: (evt, gestureState) => true,
    //   onMoveShouldSetPanResponderCapture: (evt, gestureState) => true,
    //   onPanResponderMove: (evt, gestureState) => { },
    //   onPanResponderRelease: (evt, gestureState) => {
    //     if (Math.abs(gestureState.dx) >= 50) {
    //       this.onBackspacePress();
    //     }
    //   },
    // })
  }
// Funtion to add key pressed to the display ... initial version
  handlePress = (input) => {
    const { displayValue, operator, firstValue, secondValue, nextValue } = this.state;
    switch(input){
      case '0':
      case '1':
      case '2':
      case '3':
      case '4':
      case '5':
      case '6':
      case '7':
      case '8':
      case '9':
      this.setState ({
        // displayValue: input // uum only adds one digit. Correct with below
        // displayValue: displayValue + input // append new to previous as a string
        displayValue: displayValue === '0' ? input 
        : displayValue + input // uumm. check for leading zero and remove
      })
      //below code prepares for calculation by saving values needed
      if (nextValue) {
        this.setState({
          firstValue: firstValue + input
        })
      } else {
        this.setState({
          secondValue: secondValue + input
        })
      }
      break;
      case '÷':
      case 'x':
      case '-':
      case '+':
      
        // the code below will check to see if an operator is already entered
        //if so, another operator entry is prevented and only  digit(s) accepted next
        // if user needs to enter a different operator, use the delete key
        this.setState({
          operator: input,
          nextValue: true,
          displayValue: (operator !== null) ? displayValue.substr(0, displayValue.length-1)
          : displayValue + input 
        })
        break;
      case '=' :
      
        let result = eval(firstValue + operator + secondValue)

        this.setState({
          nextValue: false,
          displayValue: result,
          firstValue: '',
          secondValue: '',
          operator: null,
        })
      default:
        break;
    }
    
    // alert(displayValue + " is new input "); //verify key pressed to debug code
  }

validate = () => {
  const { displayValue, calculationDone } = this.state;
  if(calculationDone){
    switch(displayValue.slice(-1)){
      case '/':
      case '*':
      case '-':
      case '+':
      return false;
    }
  }
  
  return true;
}
// format display with thousands comma separator using regex

formatNumber = (num) => {
  return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
}

// function used to evaluate math input in display
getAnswer = () => {
  const { displayValue  } = this.state;
  
  this.setState({
    // displayValue: eval(displayValue), // demo this line first
    displayValue: this.formatNumber(eval(displayValue)), // formats output with separator
  })
  // return this.formatNumber(displayValue)
  
}
addNumber = (input) => {
  const { displayValue, calculationDone } = this.state;
  switch(input){
    case '0':
    case '1':
    case '2':
    case '3':
    case '4':
    case '5':
    case '6':
    case '7':
    case '8':
    case '9':
    
    this.setState ({
      // displayValue: input // uum only adds one digit. Correct with below
      // displayValue: displayValue + input // append new to previous as a string
      displayValue: displayValue === '0' ? input 
      : displayValue + input // uumm. check for leading zero and remove
    })
    // below code used to determine when to reinitialize display for
    // new calculation (i.e. entering new values) or continuing the 
    // current calculation with other operators and values.
    // to introduce this concept, comment out the block of code and show 
    // how calculator malfuncitons without it developing logic reasoning
    // and need for code testing.  Thank you Jesus! Solution revealed to me
    // just as I was about to give up after one day of trial and error
    // Even with this code in place, when placed before the above setState()
    //code, it didn't work. Simply bcos that code was overiding the effect
    // of the test when it comes after it. Bingo! Do this as well for the operators
    if (calculationDone){
      // alert("I got here" + "Calculation done is:" + calculationDone)
      // this.setState(this.initialState)
      this.setState({
        displayValue:  input, 
        calculationDone: false
      })
    }
    break;
    case '.':
      // Only add a decimal point if none exists in the display
      if(displayValue.indexOf(input) === -1 ){
        this.setState({
          displayValue: displayValue + input 
        })
      }
      break;
    case '=' :
      this.setState({
        calculationDone: true
      })
      return this.validate() && this.getAnswer(); // validate operator before calling calculation funciton
      break;
  }
  
}
// function that adds math operator to input
addOperator = (input) => {
  const { displayValue, calculationDone } = this.state;
  switch(input){
    case "/" :
    case '*':
    case '-':
    case '+':
    if(displayValue == "") return
    if (calculationDone){
      this.setState({
        displayValue:  displayValue + input, 
        calculationDone: false
      })
    } else {
      if (displayValue.indexOf(input) === -1) {
        this.setState({ 
          displayValue: displayValue + input 
        });
      }
    }
    break;
  }
}
// unary button function 
onUnaryOperator = (input) => {
  const { displayValue, calculationDone } = this.state;
  switch(input){
    case 'C':
    this.setState(
      this.initialState
    )
    break;
    case '±':
    // test to see if equal sign has been presed or not before 
    // negating the display value. 
    if(calculationDone){ // this test needed after a calculation is done
      const value = String(displayValue); // corrected error by first converting value to a string
      this.setState({ //this segment throws an error without the above intervention
        displayValue: value.charAt(0) === "-" ? value.substr(1)
        : "-" + value,
        calculationDone: false
      })
    } else { // this segment works before equal sign is pressed
      this.setState({
        displayValue: displayValue.charAt(0) === "-" ? displayValue.substr(1)
        : "-" + displayValue
      })
    }
      break;
    case '%':
      const value = parseFloat(displayValue) // convert input to a number to allow division by 100
      this.setState({
        displayValue: String( value /100 ) //convert display to a % value   as a string
      })
      break;
    case 'Del':
      let displayString = displayValue.toString();
      let finalString = displayString.substr(0, displayString.length -1)
      let length = displayString.length; // variable for conditional statement below
      this.setState({
        // displayValue:  finalString // first try leaves display blank after delete
        displayValue: length == 1 ? '0' : finalString // added condition to ensure a 0 displays after deleting all input values
      });
      break;

  }
}
// Clear display and reset calculator

onClearPress = () => {
// clear the display and reset calculator variables to initial state as defined
// in the app constructor above
  this.setState(
    this.initialState
  )
}
// Add dot to the display
onDotPress = (input) => {
  const { displayValue, firstValue, secondValue, nextValue } = this.state;

  // Only add a decimal point if none exists in the display
  if(displayValue.indexOf(input) === -1 ){
    this.setState({
      displayValue: displayValue + input 
    })
  }
  //below code prepares for calculation by saving values needed
  if (nextValue) {
    this.setState({
      firstValue: firstValue + input
    })
  } else {
    this.setState({
      secondValue: secondValue + input
    })
  }

  // original way to handle above in code but still allows a dot to be netered
  // after a digit e.g. 2.4.67. (bad)

  // let dot = displayValue.slice(-1) // get the last character entered
  // this.setState({
  //   displayValue: (dot !== '.') ? displayValue + input 
  //   : displayValue
  // })


}
// Below code not working properly yet ...

onBinaryOperatorPress = (input) => {
  const { displayValue } = this.state;
    // the code below will check to see if an operator is already entered
    //if so, another operator entry is prevented and only  digit(s) accepted next
    // if user needs to enter a different operator, use the delete key
    if(displayValue.indexOf(input) === -1 ){
      this.setState({
        operator: input,
        nextValue: true,
        displayValue: displayValue + input 
      })
    }

    // alert(displayValue + " is new input "); //verify key pressed to debug code
    
    
    // Below is original code which prevents multiple operators but
    // subsequent operator entries simply deletes the display digits. Uuum?

    // this.setState ({
    //   operator: input,
    //   // displayValue: displayValue + input // append operator to input; note the display issue
      
    //   // correct input of multiple operators with the below code
    //   displayValue: (operator === null) ? displayValue + input
    //   : 
    //   displayValue.substr(0, displayValue.length -1) // append operator to input
    // })
    // break;
    // default:
    //   break;
  
}


  onEqualPress = () => {

    const { firstValue, secondValue, operator } = this.state;

    // Without the  code snippet below, you get an error when trying to multiply
    // or divide numbers since symbols used in UI button are not understood
    // by JSX. You'll get an error: "No literals allowed as numeric identifiers"
    formatOperator = (operator == "x") ?  "*" :
    (operator == "÷") ? "/" : operator
    let result = eval(secondValue + formatOperator + firstValue)

    this.setState({
      displayValue: result,
      nextValue: false,
      firstValue: '',
      secondValue: '',
      operator: null,
    })
  }

// toggle value to plus or minus
  onToggleSign = () => {
    const { displayValue } = this.state;
    this.setState({
      displayValue: displayValue.charAt(0) === "-" ? displayValue.substr(1)
      : "-" + displayValue
    })
  }

  onPercentPress = () => {
    const { displayValue,  } = this.state;
    const value = parseFloat(displayValue) // convert input to a number to allow division by 100
    this.setState({
      displayValue: String( value /100 ) //convert display to a % value to  as a string
    })
  }
 
  onDeletePress = () => {
    const { displayValue } = this.state;
    let displayString = displayValue.toString();
    let finalString = displayString.substr(0, displayString.length -1)
    let length = displayString.length; // variable for conditional statement below
    this.setState({
      // displayValue:  finalString // first try leaves display blank after delete
      displayValue: length == 1 ? '0' : finalString // added condition to ensure a 0 displays after deleting all input values
    });
  }
  

// Version 1: Good. Code is old school and not efficient but works. Start here
// for demo and seque to the array and for() loop version below this code segment.
  renderPortrait() {
    
    return (
      <View style={styles.container}>
      
        <Header headerText={'My ITF Calculator'} color={'#ffff00'} backgroundColor={'#000066'} />
        {/* <View style={{flex:1, justifyContent: "flex-end"}} {...this.panResponder.panHandlers}> */}
        <View style={{flex:1, justifyContent: "flex-end"}} >
          <Display display={this.state.displayValue} />
        </View>

        <View style={{ padding: 20 }}> 
          <View style={{flexDirection: "row", justifyContent: "space-between",}}>
            <Button onPress={() => { this.onClearPress() }} title="C" color="white" backgroundColor="red"  />
            <Button onPress={ this.onToggleSign } title="±" color="white" backgroundColor="red"  />
            <Button onPress={ this.onPercentPress } title="%" color="white" backgroundColor="red"  />
            <Button onPress={ this.onDeletePress } title="del" color="white" backgroundColor="red"   />
          </View>

          <View style={{flexDirection: "row", justifyContent: "space-between",}}>
            <Button onPress={() => { this.handlePress("7") }} title="7" color="white" backgroundColor="#607D8B"   />
            <Button onPress={() => { this.handlePress("8") }} title="8" color="white" backgroundColor="#607D8B"  />
            <Button onPress={() => { this.handlePress("9") }} title="9" color="white" backgroundColor="#607D8B"  />
            <Button onPress={() => { this.onBinaryOperatorPress("÷") }} title="÷" color="white" backgroundColor="#006600"   />
            
          </View>

          <View style={{flexDirection: "row", justifyContent: "space-between",}}>
            <Button onPress={() => { this.handlePress("4") }} title="4" color="white" backgroundColor="#607D8B"  />
            <Button onPress={() => { this.handlePress("5") }} title="5" color="white" backgroundColor="#607D8B"  />
            <Button onPress={() => { this.handlePress("6") }} title="6" color="white" backgroundColor="#607D8B"  />
            <Button onPress={() => { this.onBinaryOperatorPress("x") }} title="x" color="white" backgroundColor="#006600"   />
            
          </View>

          <View style={{flexDirection: "row", justifyContent: "space-between",}}>
            <Button onPress={() => { this.handlePress("1") }} title="1" color="white" backgroundColor="#607D8B"  />
            <Button onPress={() => { this.handlePress("2") }} title="2" color="white" backgroundColor="#607D8B"  />
            <Button onPress={() => { this.handlePress("3") }} title="3" color="white" backgroundColor="#607D8B"  />
            <Button onPress={() => { this.onBinaryOperatorPress("-") }} title="-" color="white" backgroundColor="#006600"   />
            
          </View>

          <View style={{flexDirection: "row", justifyContent: "space-between",}}>
            <Button onPress={() => { this.handlePress("0") }} title="0" color="white" backgroundColor="#607D8B"   />
            <Button onPress={ () => this.onDotPress(".") } title="." color="white" backgroundColor="#607D8B"   />
            <Button onPress={ () => this.onEqualPress() } title="=" color="white" backgroundColor="#006600"   />
            <Button onPress={() => { this.onBinaryOperatorPress("+") }} title="+" color="white" backgroundColor="#006600"   />
            
          </View>
        </View>

      </View>
    );
  }
// Version 2: Better. Code below is preferred to the one above and does the same thing
// in a more efficient way.
// 

  _renderViews = ( ) => {

    let numPad = []; // array to hold the numbers and operators

    // when we get to theming via Redux, you may want to declare 
    // btnTextColor and buttonColor as app state variables
    // if you need btnTextColor to be different for each views, declare
    //it as local var within each view as done for buttonColor below
    // even before considering it as an app state var via Redux

    let btnTextColor = "white"; // var globally declared

    for (let i=0; i < calcButtons.length; i++){
      let buttonColor = "#1b4383"; // Morgan's blue color
      let row = calcButtons[i];
      let buttonRow = [];

      for (let j=0; j < row.length; j++){
        let buttonText = row[j];

        buttonRow.push(
          <Button
            title={buttonText}
            onPress={() => this.addNumber(calcButtons[i][j])}
            color = {btnTextColor}
            backgroundColor={buttonColor}
            key={i + "-" + j} // key needed for array push
          />
        )
      }
      numPad.push(
      <View style={{ flexDirection: "row", justifyContent: "space-between",}}
        key={"row-" + i} > 
        {buttonRow} 
      </View>
      )

    }
    let unaryPad = [];
      for (let i=0; i < unaryButtons.length; i++){
        let buttonColor = "#f47937"; // morgan's orange color
        let row = unaryButtons[i];
        let buttonRow = [];

      for (let j=0; j < row.length; j++){
        let buttonText = row[j];

        buttonRow.push(
          <Button
            title={buttonText}
            onPress={() => this.onUnaryOperator(unaryButtons[i][j])}
            color = {btnTextColor}
            backgroundColor={buttonColor}
            key={j + "-"} // key needed array push
          />
        )
      }

      unaryPad.push(
        <View style={{ flexDirection: "row",  }}
          key={"row-" + i} > 
          {buttonRow} 
        </View>
        )
      }
    let opsPad = [];
      for (let i=0; i < binaryButtons.length; i++){
        let buttonColor = "#006600"; // Naija's green color
        let row = binaryButtons[i];
        let buttonRow = [];

      for (let j=0; j < row.length; j++){
        let buttonText = row[j];

        buttonRow.push(
          <Button
            title={buttonText}
            onPress={() => this.addOperator(binaryButtons[i][j])}
            color = {btnTextColor}
            backgroundColor={buttonColor}
            key={j + "-"} // key needed array push
          />
        )
      }

      opsPad.push(
        <View style={{ justifyContent: "center",}}
          key={"row-" + i} > 
          {buttonRow} 
        </View>
        )
      }
    return (
      <View style={styles.container}>
      
        <Header headerText={'My Morgan Calculator'} color={'#ffff00'} backgroundColor={'#000000'} />
        <View  style={{ flex: 2 }}>
          {/* <Text style={styles.display} > {this.state.displayValue} </Text> */}
          <Display display={this.state.displayValue} />
        </View>
        <View style={styles.unaryCont}>
          {unaryPad}
        </View>
        <View style={styles.numOpsCont}>
          <View style={ styles.numPad}>
            {numPad}
          </View>
          <View style={styles.opsCont}>
            {opsPad}
          </View>
        </View>
        <Footer />
      </View>
    )
  }
// Version 3: Best view handler (efficient and concise)
// Code  uses higher order functions for iterating array data. Preferred method

  _renderButtons = ( ) => {

    let buttonTextColor = "white"; // var globally declared
    let numPad = calcButtons.map((buttonRow, index) => {
      let buttonColor = "#1b4383"; // Morgan's blue color
      let rowItem = buttonRow.map((buttonText, buttonIndex) => {
        return <Button
          title={buttonText}
          onPress={() => this.addNumber(buttonText)}
          color = {buttonTextColor}
          backgroundColor={buttonColor}
          key={buttonIndex} // key needed for array push
        />
      })
      return <View style={{ flexDirection: "row", justifyContent: "space-between",}}
            key={index} > 
            {rowItem} 
            </View>

    })
    let unaryPad = unaryButtons.map((buttonRow, index) => {
      let buttonColor = "#ff0000"; //  red color
      // let buttonColor = "#f47937"; // Morgan's orange color
      let rowItem = buttonRow.map((buttonText, buttonIndex) => {
        return <Button
          title={buttonText}
          onPress={() => this.onUnaryOperator(buttonText)}
          color = {buttonTextColor}
          backgroundColor={buttonColor}
          key={buttonIndex} // key needed for array push
        />
      })
      return <View style={{ flexDirection: "row", justifyContent: "space-between",}}
            key={index} > 
            {rowItem} 
            </View>

    })
    let opsPad = binaryButtons.map((buttonRow, index) => {
      let buttonColor = "#006600"; // Morgan's orange color
      let rowItem = buttonRow.map((buttonText, buttonIndex) => {
        return <Button
          title={buttonText}
          onPress={() => this.addOperator(buttonText)}
          color = {buttonTextColor}
          backgroundColor={buttonColor}
          key={buttonIndex} // unique key needed to create array 
        />
      })
      // for the operators, they need to be displayed in columns (default)
      return <View style={{ justifyContent: "space-between",}} 
            key={index} > 
            {rowItem} 
            </View>

    })
    return (
      <Fragment>
      
      <SafeAreaView style={{ flex: 0, backgroundColor: '#006600' }} forceInset={{ top: 'always' }} />
      
      <SafeAreaView style={{ flex: 1, backgroundColor: '#ff0000' }} > 
      <StatusBar barStyle='light-content' />
      <View style={styles.container}>
        <Header 
          headerText={'My Naija Calculator'} 
          backgroundColor = {'#006600'}
          color = { '#fff'}
          style={[ {height: 90}, styles.androidHeader ]}
          />
        <View  style={{ flex: 2 }}>
          {/* <Text style={styles.display} > {this.state.displayValue} </Text> */}
          <Display 
            display={this.state.displayValue} 
            color="#000"
            backgroundColor= "#f8f8f8"
          />
        </View>
        <View style={styles.unaryCont}>
          {unaryPad}
        </View>
        <View style={styles.numOpsCont}>
          <View style={ styles.numPad}>
            {numPad}
          </View>
          <View style={styles.opsCont}>
            {opsPad}
          </View>
        </View>
        <Footer />
      </View>
      </SafeAreaView>
      </Fragment>
      
    )

  }

  renderLandscape() {
    return (
      <View>
        <Text>Landscape</Text>
      </View>
    )
  }

  render() {
    let view = (this.state.orientation == "portrait")
      ? this._renderButtons()
      : this.renderLandscape();

    return (
      // <ImageBackground source={ appBackgroundImage } style={{width: '100%', height: '100%'}}>

        <View style={{ flex:1,  }}>
          {view}
          {/* <Text> Calculator Screen </Text> */}
        </View>
      // </ImageBackground>
    )
  }

}

const styles = StyleSheet.create({
  androidHeader: {
    ...Platform.select({
      android: {
        paddingTop: StatusBar.currentHeight,
      }
    })
  },
  container: { 
    flex: 1, 
    // paddingVertical: 5, 
    backgroundColor: "white" // comment out to show background image
  },
  unaryCont: {
    flex: 1,
    flexDirection: "row",
    // justifyContent: "center",
    // alignItems: 'center',
    paddingHorizontal: 10, // ensures 10 spaces on both sides

  },
  
  numOpsCont: {
    flex: 4,
    flexDirection: "row",
    // justifyContent: "center",
    // alignItems: "center",
    paddingHorizontal: 10,
    paddingVertical: 10,
    bottom: 20,
  },
  numButtons: {
    flex: 3,
    // flexDirection: "row",
    // justifyContent: "space-around",
    // padding: 20,

  },
  opsCont: {
    flex: 1,
  },
   display: {
     color: 'white',
     fontSize: 70,
     alignSelf: 'flex-end',
   },
})