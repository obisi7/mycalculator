import React, { PureComponent } from 'react';
import { Text, TouchableOpacity } from 'react-native';

// Custom button that recieves buttonPress, Button Title, text style and button style

export  class Button extends PureComponent {
// use static default statement to define initial props
  static defaultProps = {
    backgroundColor: "black",
    color: "white",
    title: " ",
    radius: 35,
    style: { }
  }
  // constructor(props){
  //   super(props);
  //   this.state = {
  //     backgroundColor: "black",
  //     color: "white",
  //     title: "",
  //     radius: 40,
  //   }
  // }

  render (buttonText, buttonColor, textColor) {
    const r = this.props.radius
    const w = this.props.radius * 2 //allows button to be a circular
    const h = this.props.radius * 2 //allows button to be a circular
    const bgC = this.props.backgroundColor

    const { container, textStyle } = styles;

    return (
      <TouchableOpacity 
        onPress={this.props.onPress} 
        style={[ container, { width: w, height: h, borderRadius: r, 
          backgroundColor: bgC}, {...this.props.style } ]}>
        
        <Text style={[textStyle, { color: this.props.color }, {...this.props.style } ]}>
          {this.props.title}
        </Text>
      </TouchableOpacity>
    )

  }
}

const styles = {
  textStyle: {
    fontSize: 30,
    fontWeight: 'bold',
  },
  container: {
    alignItems: 'center',
    justifyContent: 'center',
    margin: 10,
    // padding: 2,
  }
}