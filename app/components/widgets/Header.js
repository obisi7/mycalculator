// Import libraries for making a component
import React, { Component} from 'react';
import { Text, View } from 'react-native';

// Make a component
export class Header extends Component {

  static defaultProps = {
    backgroundColor: "black",
      color: "yellow",
      headerText: "My Own Machine",
  }
  
  // constructor(props){
  //   super(props);
  //   this.state = {
  //     headerText: ""
  //   }
  // }

  render(){

    const { textStyle, viewStyle } = styles;

    return (
      <View style= { [ viewStyle, { backgroundColor: this.props.backgroundColor} ] } >
        <Text style={ [ textStyle, {color: this.props.color } ] }>{this.props.headerText}</Text>
      </View>
    ) 
  }
}

const styles = {
  viewStyle: {
    width: null,
    backgroundColor: '#F8F8F8',
    justifyContent: 'center',
    alignItems: 'center',
    height: 40, 
    paddingTop: 0,
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.2,
    elevation: 2,
    position: 'relative'
  },
  textStyle: {
    fontSize: 20,
    color: 'red'
  }
};

// Make the component available to other parts of the app
// export default Header ;