// Import libraries for making a component
import React, { Component} from 'react';
import { Text, View } from 'react-native';

// Make a component
export class Footer extends Component {

  static defaultProps = {
    backgroundColor: "white",
      color: "#000",
      footerText: "© 2019  Bisi Oladipupo ",
  }
  
  // constructor(props){
  //   super(props);
  //   this.state = {
  //     headerText: ""
  //   }
  // }

  render(){

    const { textStyle, viewStyle } = styles;

    return (
      <View style= { [ viewStyle, { backgroundColor: this.props.backgroundColor }] } >
        <Text style={[ textStyle, { color: this.props.color }, {...this.props.style } ] }>{this.props.footerText}</Text>
      </View>
    )
  }
}

const styles = {
  viewStyle: {
    width: '100%',
    backgroundColor: '#F8F8F8',
    justifyContent: 'center',
    alignItems: 'center',
    height: 15,
    
    position: 'absolute',
    bottom: 0,
    // paddingTop: 15,
    // shadowColor: '#000',
    // shadowOffset: { width: 0, height: 2 },
    // shadowOpacity: 0.2,
    // elevation: 2,
    // position: 'relative'
  },
  textStyle: {
    fontSize: 15,
    color: 'white',
    alignItems: 'center',
    justifyContent: 'center'
    // marginLeft: 5,
  }
};