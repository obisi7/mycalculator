import React, { PureComponent } from 'react';
import { View, Text } from 'react-native';
export class Display extends PureComponent {

  constructor(props){
    super(props);
    this.state = {
      display: ""
    }
  }

  render() {
    return (
      <View style={[ styles.container, { backgroundColor: this.props.backgroundColor } ]}>
          <Text 
            style={ [ styles.display, {color: this.props.color }]} > 
            {this.props.display} 
          </Text>
          
      </View>
    )
  }
}


const styles = {

  container: {
    padding: 20,
    backgroundColor: '#000066'
  },
  display: {
    color: 'white',
    fontSize: 70,
    alignSelf: 'flex-end',
  },
}
